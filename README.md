# Exceed

A very basic Excel clone, written in Haskell.

Currently it can't do much but format your "spreadsheets" and simple cell replacements,
but that's ok!

## Quick Start

``` shell
$ git clone https://gitlab.com/daisyflare/exceed
$ cd exceed
$ stack run -- <input file>
```

## Formatting 

Input files must be formatted like so:

```
a | 2 | c
1 |  | d
4 |
 |   | =A1
```

Note that there does _*not*_ have to be:
 - The same number of cells in each line
 - The pipes correctly formatted
 - Data in every cell

Note that there _*does*_ have to be:
 - Only another box-label in any equations

That above example will output: 

``` shell
$ exceed input.spreadsheet
a   | 2.0 | c
1.0 |     | d
4.0 |     |
    |     | a
```

