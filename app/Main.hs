module Main (main) where

import System.Environment as Env
import System.Exit (exitSuccess, exitFailure)
import Text.Read (readMaybe)
import Data.Char
import Data.List
import Control.Exception

main :: IO ()
main = Env.getArgs >>= parseArgs >>= print . parseSheet

parseArgs :: [String] -> IO String
parseArgs ["-h"] = putStrLn helpMessage >> exitSuccess
parseArgs ["--help"] = putStrLn helpMessage >> exitSuccess
parseArgs [file] = do
  res <- try (readFile file) :: IO (Either SomeException String)
  case res of
    Left ex -> print ex >> exitFailure
    Right ok -> return ok
parseArgs _ = putStrLn helpMessage >> exitFailure

helpMessage :: String
helpMessage = "exceed 0.1.0 \n\
              \USAGE: \n\
              \\texceed [FILE]\n\
              \OPTIONS: \n\
              \\t-h, --help\tShow this message."

newtype Sheet = Sheet
  { cells :: [[Cell]] }

instance Show Sheet where
  show s = unlines dataLines
    where
      dataLines = map (intercalate " | " . zipWith render maxLengths) $ cells s

      -- TODO: Validate equations in the spreadsheet
      -- Otherwise, if there is a situation like A1:=B1, B1:=A1, there will be a
      -- stack overflow.Additionally, equations might be referencing cells that
      -- don't exist.
      render :: Int -> Cell -> String
      render i Empty = repeatN i ""
      render i (Text t) = repeatN (i - length t) t
      render i (Eq e) = render i $ getIndex e s
      render i (Number n) = repeatN (i - length (show n)) $ show n

      repeatN :: Int -> String -> String
      repeatN 0 a = a
      repeatN i a = repeatN (i - 1) $ a ++ " "

      maxLengths = map (maxLen lens) [0..(length lens)]

      lens = map (map len) $ cells s

      maxLen :: [[Int]] -> Int -> Int
      maxLen l i = foldl max 0 $ map (!! i) l

      len :: Cell -> Int
      len c = case c of
                Eq e -> len $ getIndex e s
                Text t -> length t
                Number n -> length $ show n
                Empty -> 1

getIndexSplit :: (String, Int) -> Sheet -> Cell
getIndexSplit (s_i, i_i) s = (cells s !! (i_i - 1)) !! (stringIdx - 1)
  where
    stringIdx :: Int
    stringIdx = sum
      $ zipWith (*) (map (\x -> 26^(x-1))
             $ reverse [1..(length s_i)])
                    $ map ((\x -> x - 64) . ord . toUpper) s_i

getIndex :: String -> Sheet -> Cell
getIndex idx = getIndexSplit (stringPart, numPart)
  where
    stringPart = takeWhile (not . isDigit) idx
    numPart = read $ reverse $ takeWhile isDigit $ reverse idx

parseSheet :: String -> Sheet
parseSheet raw = Sheet cells
  where
    cells = map (makeLen longestLen) rawCellList

    longestLen = foldl max 0 $ map length rawCellList

    rawCellList = map (map (parseCell . trim) . split (== '|')) $ lines raw

    makeLen :: Int -> [Cell] -> [Cell]
    makeLen len l = addLen (len - length l) l

    addLen :: Int -> [Cell] -> [Cell]
    addLen 0 l = l
    addLen (-1) _ = undefined
    addLen i l = addLen (i - 1) l ++ [Empty]

    parseCell :: String -> Cell
    parseCell "" = Empty
    parseCell ('=':rest) = Eq rest
    parseCell s = case (readMaybe :: String -> Maybe Float) s of
      Just i -> Number i
      Nothing -> Text s

    split :: (a -> Bool) -> [a] -> [[a]]
    split func list = splitInner func list []
      where splitInner :: (a -> Bool) -> [a] -> [[a]] -> [[a]]
            splitInner _ [] acc = acc
            splitInner f l acc = splitInner f (maybeTail $ dropWhile (not . f) l) (acc ++ [takeWhile (not . f) l])

            maybeTail :: [a] -> [a]
            maybeTail [] = []
            maybeTail (_:xs) = xs

    trim :: String -> String
    trim = reverse . dropWhile isSpace . reverse . dropWhile isSpace

data Cell = Empty | Text String | Number Float | Eq String
  deriving Show
